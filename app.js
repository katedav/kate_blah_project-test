// -------------------------------------------------------------------------
// Setup Express / Handlebars / Body-Parser / fs / cookie-parser / express-session 
// -------------------------------------------------------------------------
var express = require("express");
var app = express();
app.set("port", process.env.PORT || 3000);

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({ defaultLayout: "main-layout" }));
app.set("view engine", "handlebars");

// Add "fs" module
var fs = require("fs");

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));

// User cookie-parser for our cookies
var cookieParser = require("cookie-parser");
app.use(cookieParser());

// Specify that the app should use express-session to create in-memory sessions
var session = require("express-session");
app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: "compsci719"
  })
);

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

getUser = function (username, callback) {
  module.exports.username = username.toLowerCase(); // Exporting our user so that our dao-modules.js can access it. 
  DAO.getUser(function (user) {
    callback(user);
  });
};

var localStrategy = new LocalStrategy(
  function (username, password, done) {

    getUser(username, function (user) {

      if (!user) {
        return done(null, false, { message: 'Invalid user' });
      };

      if (user.password !== password) {
        return done(null, false, { message: 'Invalid password' });
      };
      done(null, user);
    });
  }
);

passport.serializeUser(function (user, done) {
  done(null, user.username);
});

passport.deserializeUser(function (username, done) {
  getUser(username, function (user) {
    done(null, user);
  });
});

passport.use('local', localStrategy);

app.use(passport.initialize());
app.use(passport.session());

// Use our custom DAO module
var DAO = require('./dao-module.js');

// -------------------------------------------------------------------------
// HELPER FUNCTIONS
// -------------------------------------------------------------------------
// Helper function to make sure that the returned result is an array.
function ensureArray(value) {
  if (value == undefined) return [];
  else return Array.isArray(value) ? value : [value];
}

function checkArticleOwner(articles, username) {
  for (var i = 0; i < articles.length; i++) {
      
    if (articles[i].username == username) {
      articles[i]["isOwner"] = true ; 
    }

  }
}

// -------------------------------------------------------------------------
// FUNCTIONS END
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ROUTE HANDLERS
// -------------------------------------------------------------------------

app.get('/', function (req, res) {

  if (req.isAuthenticated()) {
    var username = req.user.username;
  }

  // Created to separate tasks between team members. Designing the popups will mean you only focus on these files. 
  var addArticlePopup = fs.readFileSync(__dirname + "/views/addArticle-popup.handlebars", "utf8");
  var loginPopup = fs.readFileSync(__dirname + "/views/login-popup.handlebars", "utf8");
  var signupPopup = fs.readFileSync(__dirname + "/views/signup-popup.handlebars", "utf8");
  var profilePop = fs.readFileSync(__dirname + "/views/profile-popup.handlebars", "utf8");
  let userDetails ; 

  DAO.getUser(function(user) {
    userDetails = user
  }); 

  var code = {
    addArticlePopup: addArticlePopup,
    loginPopup: loginPopup,
    signupPopup: signupPopup,
    profilePop: profilePop 
  };

  DAO.getAllArticles(function (articles) {

    checkArticleOwner(articles,username) ; // Puts a 'isowner: true' in the article object if the username is the owner of that article
      var data = {
        articles: articles,
        userDetails: userDetails,
        code: code,
        username: username
      }; 

    res.render("home", data );
  });

});

// When the user POSTs to "/add", add a new article, then redirect back to articles display.
app.post('/add', function (req, res) {

  if (req.isAuthenticated()) {
    var username = req.user.username;
  }

  var articleTitle = req.body.title;
  var articleContent = req.body.content;

  DAO.addArticle(articleTitle, articleContent, username, function (id) {

    console.log("New article added with id = " + id);

    res.redirect("/");
  });
});

app.post('/login', passport.authenticate('local',
    {
        successRedirect: '/',
        failureRedirect: '/login?loginFail=true'
    }
));

app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/");
});

app.get('/delete/:articleID', function (req, res) {

  var articleId = req.params.articleID;
  DAO.deleteArticle(articleId, function (rowsAffected) {

      console.log(rowsAffected + " row(s) affected.");

      res.redirect('/');
  });
});

app.post('/signup', (req,res) => {
    let username = req.body.username; 
    var password = req.body.psw ; 
    var fname = req.body.fname ; 
    var lname = req.body.lname ; 
    var dob = req.body.dob ; 
    var country = req.body.country ; 
    var avatar = req.body.avatar ;

    DAO.addUser(username, password, fname, lname, dob, country, avatar, function(id) {

      console.log("New User added with id = " + id);
      res.redirect("/") ;

    });

})



// -------------------------------------------------------------------------
// ROUTE HANDLER END
// -------------------------------------------------------------------------

// Serve "public" files
app.use(express.static(__dirname + "/public"));

// Start the server running.
app.listen(app.get("port"), function () {
  console.log("Express started on http://localhost:" + app.get("port"));
});
