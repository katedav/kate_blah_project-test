// Allow us to use SQLite3 from node.js
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blah.db');
var User = require('./app.js');

module.exports.getUser = function (callback) {
    db.all('SELECT * FROM users WHERE username = ?', [User.username], function (err, rows) {
        if (rows.length > 0) {
            callback(rows[0]);
        } else {
            callback(null);
        }
    });
}

// A function which loads all articles from the database, then sends them to the given callback.
module.exports.getAllArticles = function (callback) {
    db.all("SELECT articleId, title, substr(content, 1, 100) || '...' as 'content', username FROM articles", function (err, rows) {

        // console.log(rows);

        // "rows" is a JavaScript array, and we can do anything we would normally do for such an array.
        callback(rows);
    });
};

// A function which gets the article with the given id from the database, then sends it to the given callback.
module.exports.getArticle = function (id, callback) {
    db.all("SELECT * FROM articles WHERE id = ?", [id], function (err, rows) {

        var article = rows[0]; // There should only be one...

        callback(article);
    });
};

// A function that adds a new article with the given title and content to the database, then sends its automatically generated id to the given callback.
module.exports.addArticle = function (title, content, username, callback) {
    db.run("INSERT INTO articles (title, content,username) VALUES (?,?,?)", [title, content, username], function (err) {

        // Within this function, you can use this.lastID to get the automatically-generated ID that was just inserted into the db.
        // console.log("New article added with id = " + this.lastID);

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        // console.log(this.changes + " row(s) affected.");

        callback(this.lastID);
    });
};

// A function that deletes the article with the given id from the database, then sends the number of rows affected (probably 0 or 1) to the given callback.
module.exports.deleteArticle = function (id, callback) {
    console.log(id); 


    db.run("DELETE FROM articles WHERE articleID = ?", [id], function (err) {

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        // console.log(this.changes + " row(s) affected.");

        callback(this.changes);
    });
};

module.exports.addUser = function (username, password, fname, lname, dob, country, avatar, callback) {
    console.log(username) ;

    db.run("INSERT INTO users (username,password,fname, lname, dob, country, avatar) VALUES (?,?,?,?,?,?,?)", [username,password, fname, lname, dob, country,avatar], function (err) {
        
        if (err) console.log(err);
        callback("User added"); 
        
    });
};